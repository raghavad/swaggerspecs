const fs = require('fs');
const path = require('path');
const YAML = require('yamljs');
const SCHEMA_DIR = './schemas';
const getDirectories = srcPath => fs.readdirSync(srcPath).filter(file =>
    fs.statSync(path.join(srcPath, file)).isDirectory())

const directories = getDirectories(SCHEMA_DIR);
for (const folder of directories) {
    console.log(folder);
    const swaggerPath = path.join(SCHEMA_DIR, folder, 'swagger.yml');
    console.log(swaggerPath);
    const swaggerJsonPath = path.join(SCHEMA_DIR, folder, 'swagger.json');
    if(fs.existsSync(swaggerPath)){
        const jsonObject = YAML.load(swaggerPath);
        fs.writeFileSync(swaggerJsonPath, JSON.stringify(jsonObject, null, 2));
        console.log(`processed ${swaggerPath}`);
    }
}